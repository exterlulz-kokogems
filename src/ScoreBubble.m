//
//  ScoreBubble.m
//  jeweltoy
//
//  Created by Mike Wessler on Sat Jun 15 2002.
//
/*
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ----====----====----====----====----====----====----====----====----====---- */

#import "ScoreBubble.h"

#import "OpenGLSprite.h"

NSMutableDictionary *stringAttributes;

// Open GL Z value for scorebubbles
#define SCOREBUBBLE_SPRITE_Z	-0.30

@implementation ScoreBubble

@synthesize value           = _value;
@synthesize screenLocation  = _screenLocation;
@synthesize animationCount  = _animationCount;
@synthesize image           = _image;

+ (ScoreBubble *)scoreWithValue:(int)value at:(NSPoint)location duration:(int)count
{
  // FIXME: possible bug, is it [self alloc] or [[self class] alloc]???
  ScoreBubble *scoreBubble = [[self alloc] initWithValue:value
                                                      at:location
                                                duration:count];
  return [scoreBubble autorelease];
}

-(id)initWithValue:(int)value at:(NSPoint)location duration:(int)count
{
    NSString *str= [NSString stringWithFormat:@"%d", value];
    NSSize strsize;
  self = [super init];
    if (self != nil) {
	if (!stringAttributes) {
	    stringAttributes= [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSFont fontWithName:@"ArialNarrow-Bold" size:18],
		NSFontAttributeName, [NSColor blackColor], NSForegroundColorAttributeName, NULL];
	    [stringAttributes retain];
	}
	strsize= [str sizeWithAttributes:stringAttributes];
	strsize.width+= 3;
	strsize.height+=1;
	_value= value;
	_screenLocation = location;
	_screenLocation.x -= strsize.width / 2;
	_screenLocation.y -= strsize.height / 2;
	_animationCount= count;
	_image = [[NSImage alloc] initWithSize:strsize];
	[_image lockFocus];
	[stringAttributes setObject:[NSColor blackColor] forKey:NSForegroundColorAttributeName];	
	[str drawAtPoint:NSMakePoint(2,0) withAttributes:stringAttributes];
	[stringAttributes setObject:[NSColor yellowColor] forKey:NSForegroundColorAttributeName];	
	[str drawAtPoint:NSMakePoint(1,1) withAttributes:stringAttributes];
	[_image unlockFocus];

        // Open GL
      NSSize imageSize = _image.size;
        sprite = [[OpenGLSprite alloc] initWithImage:_image
                                       cropRectangle:NSMakeRect(0, 0, imageSize.width, imageSize.height)
                                                size:imageSize];
    }
  
    return self;
}

- (void)dealloc
{
  [_image release];
  [sprite release];
  
  [super dealloc];
}

-(void)drawImage
{
    float alpha = (float)_animationCount/20;
    if (alpha>1) {
        alpha= 1;
    }
  
    [_image compositeToPoint:_screenLocation
                  operation:NSCompositeSourceOver
                   fraction:alpha];
}

-(void)drawSprite
{
    float alpha= (float)_animationCount/20;
    if (alpha>1) {
        alpha= 1;
    }
    [sprite blitToX:_screenLocation.x
                  Y:_screenLocation.y
                  Z:SCOREBUBBLE_SPRITE_Z
              Alpha:alpha];
}

-(int)animate
{
  if (_animationCount > 0) {
    _screenLocation.y++;
    _animationCount--;
  }
  
  return _animationCount;
}

@end
