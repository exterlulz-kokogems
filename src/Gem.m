/* ----====----====----====----====----====----====----====----====----====----
Gem.m (jeweltoy)

JewelToy is a simple game played against the clock.
Copyright (C) 2001  Giles Williams

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
----====----====----====----====----====----====----====----====----====---- */


#import "Gem.h"

// Open GL
#import "OpenGLSprite.h"

@implementation Gem

@synthesize gemType           = _gemType;
@synthesize image             = _image;
@synthesize sprite            = _sprite;
@synthesize tinkSound         = _tinkSound;
@synthesize sploinkSound      = _sploinkSound;
@synthesize state             = _state;
@synthesize animationCounter  = _animationCounter;

- (id)init
{
  self = [super init];
  if (self != nil) {
    // TODO: replace @"tink" and @"sploink" with static const NSString*
    // TODO: retain sounds?
    _tinkSound    = [NSSound soundNamed:@"tink"];
    _sploinkSound = [NSSound soundNamed:@"sploink"];
    
    // MW...
    waitForFall= 0;
  }
  
  return self;
}

- (void)dealloc {
    [super dealloc];
}

+ (Gem *)gemWithNumber:(int)d andImage:(NSImage *)img
{
  Gem	*gem = [[Gem alloc] init];

  gem.gemType = d;
  gem.image = img;
  gem.tinkSound     = [NSSound soundNamed:@"tink"];
  gem.sploinkSound  = [NSSound soundNamed:@"sploink"];
  
  return [gem autorelease];
}

+ (Gem *)gemWithNumber:(int)d andSprite:(OpenGLSprite *)sprite
{
  Gem	*gem = [[Gem alloc] init];
  
  gem.gemType = d;
  gem.sprite = sprite;
  gem.tinkSound     = [NSSound soundNamed:@"tink"];
  gem.sploinkSound  = [NSSound soundNamed:@"sploink"];
  
  return [gem autorelease];
}

- (int) animate
{
    if (_state == GEMSTATE_RESTING)
    {
        [self setPositionOnScreen:positionOnBoard.x*48:positionOnBoard.y*48];
        _animationCounter = 0;
    }
    if (_state == GEMSTATE_FADING)
    {
        [self setPositionOnScreen:positionOnBoard.x*48:positionOnBoard.y*48];
        if (_animationCounter > 0)
          _animationCounter--;
    }
    // MW...
    if (_state== GEMSTATE_SHIVERING) {
        positionOnScreen.x= positionOnBoard.x*48+(rand()%3)-1;
        positionOnScreen.y= positionOnBoard.y*48;
    }
    //
    if (_state == GEMSTATE_FALLING)
    {
        if (_animationCounter < waitForFall) {
            positionOnScreen.x= positionOnBoard.x*48;
            //positionOnScreen.y= positionOnBoard.y*48;
            _animationCounter++;
        }
        else if (positionOnScreen.y > (positionOnBoard.y*48))
        {
            positionOnScreen.y += vy;
            positionOnScreen.x = positionOnBoard.x*48;
            vy -= GRAVITY;
            _animationCounter++;
        }
        else
        {
			[_tinkSound play];
            positionOnScreen.y = positionOnBoard.y * 48;
            _state = GEMSTATE_RESTING;
        }
    }
    if (_state == GEMSTATE_SHAKING)
    {
        positionOnScreen.x = positionOnBoard.x*48+(rand()%5)-2;
        positionOnScreen.y = positionOnBoard.y*48+(rand()%5)-2;
        if (_animationCounter > 1) _animationCounter--;
        else _state = GEMSTATE_RESTING;
    }
    if (_state == GEMSTATE_ERUPTING)
    {
        if (positionOnScreen.y > -48)
        {
            if (_animationCounter < GEM_ERUPT_DELAY)
            {
                positionOnScreen.x = positionOnBoard.x*48+(rand()%5)-2;
                positionOnScreen.y = positionOnBoard.y*48+(rand()%5)-2;
            }
            else
            {
                positionOnScreen.y += vy;
                positionOnScreen.x += vx;
                vy -= GRAVITY;
            }
            _animationCounter++;
        }
        else
        {
          _animationCounter = 0;
        }
    }
    if (_state == GEMSTATE_MOVING)
    {
        if (_animationCounter > 0)
        {
            positionOnScreen.y += vy;
            positionOnScreen.x += vx;
            _animationCounter--;
        }
        else _state = GEMSTATE_RESTING;
    }
    return _animationCounter;
}

- (void)fade
{
  [_sploinkSound play];
  
  _state = GEMSTATE_FADING;
  _animationCounter = FADE_STEPS;
}

- (void)fall
{
  _state = GEMSTATE_FALLING;
  // MW...
  waitForFall= rand()%6;
  
  vx = 0;
  vy = 0;
  _animationCounter = 1;
}

// MW...
- (void) shiver
{
  _state= GEMSTATE_SHIVERING;
  _animationCounter = 0;
}

- (void) shake
{
  _state = GEMSTATE_SHAKING;
  _animationCounter = 25;
}

- (void) erupt
{
  [self setVelocity:(rand()%5)-2:(rand()%7)-2:1];
  
  _state = GEMSTATE_ERUPTING;
  _animationCounter = GEM_ERUPT_DELAY;
}

- (void) drawImage
{
    if (_state == GEMSTATE_FADING)
        [_image compositeToPoint:[self positionOnScreen] operation:NSCompositeSourceOver fraction:(_animationCounter / FADE_STEPS)];
    else
        [_image compositeToPoint:[self positionOnScreen] operation:NSCompositeSourceOver];
}

- (void) drawSprite
{
    if (_state == GEMSTATE_FADING)
        [[self sprite] blitToX:positionOnScreen.x
                             Y:positionOnScreen.y
                             Z:GEM_SPRITE_Z
                         Alpha:(_animationCounter / FADE_STEPS)];
    else
        [[self sprite] blitToX:positionOnScreen.x
                             Y:positionOnScreen.y
                             Z:GEM_SPRITE_Z
                         Alpha:1.0];
}

- (NSPoint) positionOnScreen
{
    return positionOnScreen;
}
- (void) setPositionOnScreen:(int) valx :(int) valy
{
    positionOnScreen.x = valx;
    positionOnScreen.y = valy;
}

- (void) setVelocity:(int) valx :(int) valy :(int) steps
{
    vx = valx;
    vy = valy;
  
    _animationCounter = steps;
    _state = GEMSTATE_MOVING;
}

- (NSPoint) positionOnBoard {
    return positionOnBoard;
}

- (void)setPositionOnBoard:(int) valx :(int) valy
{
    positionOnBoard.x = valx;
    positionOnBoard.y = valy;
}

@end
