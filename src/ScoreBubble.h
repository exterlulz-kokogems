//
//  ScoreBubble.h
//  jeweltoy
//
//  Created by Mike Wessler on Sat Jun 15 2002.
//
/*
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ----====----====----====----====----====----====----====----====----====---- */

#import <Cocoa/Cocoa.h>

@class OpenGLSprite;

@interface ScoreBubble : NSObject
{
  int _value;
  NSPoint _screenLocation;
  int _animationCount;
  NSImage *_image;
  
  OpenGLSprite	*sprite;
}

// TODO: remove, not used?
@property (readonly) int value;
@property (readonly) NSPoint screenLocation;
@property int animationCount;
@property (readonly, assign) NSImage *image;

+ (ScoreBubble *)scoreWithValue:(int)value at:(NSPoint)location duration:(int)count;

- (id)initWithValue:(int)value at:(NSPoint)location duration:(int)count;

- (void)drawImage;
- (void)drawSprite;

- (int)animate;

@end
